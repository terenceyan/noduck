//
//  DuckViewController.swift
//  NoDuck
//
//  Created by Terence on 2017-02-20.
//  Copyright © 2017 Terence Yan. All rights reserved.
//

import UIKit

class DuckViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    
    @IBAction func addWord(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Add word", message: "Enter a word to teach the keyboard.", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Fuckity fuck"
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if let input = alert.textFields?.first?.text {
                self.add(word: input)
                self.statusLabel.text = "Just learned \(input)!"
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationItem.title = "No Duck"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadWords()
    }
    
    func loadWords() {
        guard let defaultWordList = Bundle.main.path(forResource: "DuckWords", ofType: "plist"), defaultWordList != "" else {
            print("Cannot build plist path")
            return;
        }
        
        guard let dict = NSDictionary.init(contentsOfFile: defaultWordList) as? [String: AnyObject], dict.count > 0 else {
            print("Cannot find plist file to dictionary")
            return;
        }

        if let array = dict["Words"] as? [String] {
            array.forEach { (word) in
                add(word: word)
            }
           
        }
    }
    
    func add(word: String) {
        if !UITextChecker.hasLearnedWord(word) {
            UITextChecker.learnWord(word)
        }
    }
}

